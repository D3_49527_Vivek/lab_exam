 use Exam;
switched to db Exam
> db;
Exam
> show collections;
> show databases;
admin   0.000GB
config  0.000GB
local   0.000GB
> db;
Exam
> db.Emps.insert({ Emp_id : 1, Name : "Abhishek", City : "Mumbai", State : "MH", Salary : "12000",Dept_id : 1});
WriteResult({ "nInserted" : 1 })
> db.Emps.insert({ Emp_id : 2, Name : "Dipak", City : "Pune", State : "MH", Salary : "15000",Dept_id : 2});
WriteResult({ "nInserted" : 1 })
> db.Emps.insert({ Emp_id : 3, Name : "Ravi", City : "Surat", State : "Gujrat", Salary : "7000",Dept_id : 1});
WriteResult({ "nInserted" : 1 })
> db.Emps.find();
{ "_id" : ObjectId("60a77fec042bfbe8253f57dc"), "Emp_id" : 1, "Name" : "Abhishek", "City" : "Mumbai", "State" : "MH", "Salary" : "12000", "Dept_id" : 1 }
{ "_id" : ObjectId("60a781df042bfbe8253f57dd"), "Emp_id" : 2, "Name" : "Dipak", "City" : "Pune", "State" : "MH", "Salary" : "15000", "Dept_id" : 2 }
{ "_id" : ObjectId("60a78214042bfbe8253f57de"), "Emp_id" : 3, "Name" : "Ravi", "City" : "Surat", "State" : "Gujrat", "Salary" : "7000", "Dept_id" : 1 }
> db.Emps.insert({ Emp_id : 4, Name : "Radha", City : "Delhi", State : "Delhi", Salary : "19000",Dept_id : 1});
WriteResult({ "nInserted" : 1 })
> db.Emps.insert({ Emp_id : 5, Name : "Suman", City : "Panjim", State : "Goa", Salary : "14000",Dept_id : 3});
WriteResult({ "nInserted" : 1 })
> db.Emps.insert({ Emp_id : 6, Name : "Adhik", City : "Satara", State : "MH", Salary : "10000",Dept_id : 2});
WriteResult({ "nInserted" : 1 })
> db.Emps.find();
{ "_id" : ObjectId("60a77fec042bfbe8253f57dc"), "Emp_id" : 1, "Name" : "Abhishek", "City" : "Mumbai", "State" : "MH", "Salary" : "12000", "Dept_id" : 1 }
{ "_id" : ObjectId("60a781df042bfbe8253f57dd"), "Emp_id" : 2, "Name" : "Dipak", "City" : "Pune", "State" : "MH", "Salary" : "15000", "Dept_id" : 2 }
{ "_id" : ObjectId("60a78214042bfbe8253f57de"), "Emp_id" : 3, "Name" : "Ravi", "City" : "Surat", "State" : "Gujrat", "Salary" : "7000", "Dept_id" : 1 }
{ "_id" : ObjectId("60a782a9042bfbe8253f57df"), "Emp_id" : 4, "Name" : "Radha", "City" : "Delhi", "State" : "Delhi", "Salary" : "19000", "Dept_id" : 1 }
{ "_id" : ObjectId("60a78309042bfbe8253f57e0"), "Emp_id" : 5, "Name" : "Suman", "City" : "Panjim", "State" : "Goa", "Salary" : "14000", "Dept_id" : 3 }
{ "_id" : ObjectId("60a78344042bfbe8253f57e1"), "Emp_id" : 6, "Name" : "Adhik", "City" : "Satara", "State" : "MH", "Salary" : "10000", "Dept_id" : 2 }
>


d3_49527>show databases;
+--------------------+
| Database           |
+--------------------+
| classwork          |
| hr                 |
| information_schema |
| joins              |
| northwind          |
| sales              |
| spj                |
+--------------------+
7 rows in set (0.00 sec)

d3_49527>use classwork;
Database changed
d3_49527>show table;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 1
d3_49527>show tables;
+---------------------+
| Tables_in_classwork |
+---------------------+
| addr                |
| bonus               |
| books               |
| dept                |
| dummy               |
| emp                 |
| emp_meeting         |
| emps                |
| meeting             |
| salgrade            |
| student             |
+---------------------+
11 rows in set (0.10 sec)

d3_49527>Create table Dept_tb (Dept_Id ,Dept varchar(15));
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ',Dept varchar(15))' at line 1
d3_49527>Create table Dept_tb (Dept_Id int ,Dept varchar(15));
Query OK, 0 rows affected (1.46 sec)

d3_49527>insert into Dept_tb values(1,Electronis);
ERROR 1054 (42S22): Unknown column 'Electronis' in 'field list'
d3_49527>drop table Dept_tb;
Query OK, 0 rows affected (0.62 sec)

d3_49527>Create table Dept_tb (Dept_Id int ,Dept char(15));
Query OK, 0 rows affected (0.83 sec)

d3_49527>insert into Dept_tb values(1,"Electronis");
Query OK, 1 row affected (0.15 sec)

d3_49527>insert into Dept_tb values(2,"Civil");
Query OK, 1 row affected (0.17 sec)

d3_49527>insert into Dept_tb values(3,"Chemical");
Query OK, 1 row affected (0.12 sec)

d3_49527>show table;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 1
d3_49527>show tables;
+---------------------+
| Tables_in_classwork |
+---------------------+
| addr                |
| bonus               |
| books               |
| dept                |
| dept_tb             |
| dummy               |
| emp                 |
| emp_meeting         |
| emps                |
| meeting             |
| salgrade            |
| student             |
+---------------------+
12 rows in set (0.08 sec)

d3_49527>select *from dept_tb;
+---------+------------+
| Dept_Id | Dept       |
+---------+------------+
|       1 | Electronis |
|       2 | Civil      |
|       3 | Chemical   |
+---------+------------+
3 rows in set (0.00 sec)